# README #

This is a mini router application so you can code more comfortable with smaller projects where you don't want to install a full copy of laravel. Note that it's only a router that is laravel like, so you might miss some functionality, if you want me to build more functionality, please send me a message or create an issue.

Ofcourse, pull-requests are welcome aswell.


### How do I get set up? ###

You actually don't have to do a whole lot to get up and running, these are the steps:

* Create a vhost to the public folder
* Modify your templates
* Create routes in the routes file
* Create a controller (or point the routes to the existing PagesController)
* Create a controller method
* Return a view